# require 'dotenv'
# Dotenv.load('.env')
# CUSTOM MODULE TO ACCESS QISCUS API OR THINGS RELATED TO QISCUS
# ------------------------------------------------------------- #
module Qiscus

  # QISCUS API V1 (METHOD GET) #

  def self.get_api_v1(url, room)
    res = HTTP.headers(:accept => "application/json", "Qiscus-App-Id" => room.app.app_code, "Qiscus-Secret-Key" => room.app.app_secret).get(room.app.multichannel_url + "/api/v1/" + url)
    data_object = nil
    if res.code == 200
      data_object = JSON.parse(res.body).to_dot
    else
      self.loge("error finding room_id")
    end
    data_object
  end

  # QISCUS API V2 (METHOD GET) #

  def self.get_api_v2(url, room) # ==> url and room are both input parameters, self here means using this module to call this function.
    # this might be used to store the response from HTTP get process into res.
    res = HTTP.headers(:accept => "application/json", "Qiscus-App-Id" => room.app.app_code, "Qiscus-Secret-Key" => room.app.app_secret).get(room.app.multichannel_url + "/api/v2/" + url)
    data_object = nil
    if res.code == '200'
      data_object = JSON.parse(res.body).to_dot
    else
      self.loge("error finding room_id")
    end
    data_object
  end
  # ----------BY APP------------ #
  def self.get_api_v2_by_app(url, app)
    res = HTTP.headers(:accept => "application/json", "Qiscus-App-Id" => app.app_code, "Qiscus-Secret-Key" => app.app_secret).get(app.multichannel_url + "/api/v2/" + url)
    data_object = nil
    if res.code == 200
      data_object = JSON.parse(res.body).to_dot
    else
      self.loge("error finding room_id")
    end
    data_object
  end

  # QISCUS API V2 (METHOD POST) #

  def self.post_api_v2(url, data, room)
    res = HTTP.headers(:accept => "application/json", "Qiscus-App-Id" => room.app.app_code, "Qiscus-Secret-Key" => room.app.app_secret).post(room.app.multichannel_url + "/api/v2/" + url, :form => data)
    data_object = nil
    if res.code == 200
      data_object = JSON.parse(res.body).to_dot
    else
      self.loge("error finding room_id")
    end
    data_object
  end

  # QISCUS API V1 (METHOD POST) #

  def self.post_api_v1(url, payload, data)
    app_secret = "#{ENV["QISCUS_APP_SECRET"]}"
    res = HTTP.headers(:accept => "application/json", "Qiscus-App-Id" => data.app_id, "Qiscus-Secret-Key" => app_secret).post("#{ENV["QISCUS_URL"]}/api/v1/" + url, :form => payload)
    data_object = nil
    if res.code == 200
      data_object = JSON.parse(res.body).to_dot
    else
      self.loge("error finding room_id")
    end
    data_object
  end

  def self.logi(log)
    puts log
    agent_logger ||= Logger.new("#{Rails.root}/log/agent_allocations.log")
    agent_logger.info log
  end

  def self.loge(log)
    puts log
    agent_logger ||= Logger.new("#{Rails.root}/log/agent_allocations.log")
    agent_logger.error log
  end

  def self.assign_agent(data)

    # * ASSIGN AGENT TO MULTICHANNEL * #

    assign_agent = self.post_api_v1('admin/service/assign_agent',{ 'room_id' => data.room_id, 'agent_id' => data.candidate_agent.id }, data)
    if assign_agent
      self.logi("Qiscus Room ID " + data.room_id.to_s + " assigned to " + assign_agent.to_s + " with last reserved customer = ?")
    else
      self.logi("Room with id " + data.room_id.to_s + " is still waiting because can't assign agent")
    end
  end

  def self.send_message_text(data, message)
    app_secret = "#{ENV["QISCUS_APP_SECRET"]}"
    payload = { 'sender_email' => "#{ENV["QISCUS_EMAIL_ADMIN"]}", 'message' => message, 'type' => "text", "room_id" => data.room_id }
    res = HTTP.headers(:accept => "application/json", "Qiscus-Secret-Key" => app_secret).post("#{ENV["QISCUS_URL"]}/#{data.app_id}/bot", :form => payload)
    data_object = nil
    if res.code == 200
      data_object = JSON.parse(res.body).to_dot
    else
      self.loge("error finding room_id")
    end
    data_object
  end

end