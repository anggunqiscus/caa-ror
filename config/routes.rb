Rails.application.routes.draw do
  get '/', to: 'app#landing'
  post 'caa', to: 'qiscus#assign_agent'
  get 'caa', to: 'qiscus#assign_agent'
  get 'apps', to: 'app#index'
  get 'healthcheck', to: 'app#monitoring'
  post 'apps/register', to: 'app#register'
end
