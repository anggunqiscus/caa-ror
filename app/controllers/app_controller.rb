
class AppController < ApplicationController
  # HEALTHCHECK
  def monitoring
    render json: { ok: true }, status: :ok
  end

  # SHOWING MESSAGE
  def landing
    render json: { message: "Simple CAA Trial with RoR " }, status: :ok
  end

  # REGISTER APP
  def register
    params.require([:app_code, :app_secret, :multichannel_url])
    app = App.find_by_app_id(params[:app_code])
    if app
      app.app_secret = params[:app_secret]
      app.multichannel_url = params[:multichannel_url]
      app.save
      render json: { message: "App exists, updating configuration" }, status: :created
    else
      app = App.create(app_code: params[:app_code], app_secret: params[:app_secret], multichannel_url: params[:multichannel_url], threshold: 10)
      render json: { message: "App created" }, status: :created
    end
  end
end