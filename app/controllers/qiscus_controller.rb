require 'helpers/qiscus'

class QiscusController < ApplicationController
  def assign_agent
    data = JSON.parse(request.body.read).to_dot
      Qiscus.logi(data)
      if data.candidate_agent == nil
        Qiscus.logi("Agent is not available")
        # Qiscus.send_message_text(data, "Maaf, saat ini agent tidak tersedia. Mohon menunggu hingga agen membalas pesan Anda.")
      else
        Qiscus.assign_agent(data)
        # Qiscus.send_message_text(data, "Selamat datang di Qiscus caa-ror-trial. Ada yang bisa dibantu?")
      end
    end
end